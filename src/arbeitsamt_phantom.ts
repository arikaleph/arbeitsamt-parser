import cheerio = require('cheerio');
import puppeteer = require('puppeteer');

(async () => {

    const browser = await puppeteer.launch({headless: true});
    const page = await browser.newPage();
    await page.setViewport({width: 800, height: 1024});

    {
        await page.goto('https://anmeldung.arbeitsagentur.de/portal', {waitUntil: 'networkidle2'});
        await page.screenshot({path: __dirname + '/../out/load_01.png'});
    }

    {
        // fill in the password form
        await page.evaluate('document.getElementById(\'pt1:r1:0:pt1:s1:dc1:ANM_TfBn::content\').value = \'aeropor5\';');
        await page.evaluate('document.getElementById(\'pt1:r1:0:pt1:s1:dc2:ANM_TfKw::content\').value = \'Pflegedirektor17\';');
        await page.screenshot({path: __dirname + '/../out/load_02.png'});
    }

    {
        // press the login button
        // await page.evaluate('document.getElementById(\'pt1:r1:0:pt1:s1:ANM_BtAnm\').click();');
        await page.click('button#pt1\\:r1\\:0\\:pt1\\:s1\\:ANM_BtAnm');

        // await page.waitFor(5000); // wait 3 seconds because arbeitsamt has delays before initiating requests
        await page.waitForNavigation({waitUntil: 'networkidle0', timeout: 0}); // wait for the network requests to settle once again
        await page.screenshot({path: __dirname + '/../out/load_03.png'});
    }

    {
        // navigate to offers
        await page.goto('https://jobboerse.arbeitsagentur.de/vamJB/stellenangeboteVerwalten.html?execution=e1s1');
        await page.waitFor(3000); // wait 3 seconds because arbeitsamt has delays before initiating requests

        // the first time we try, it gets redirected back
        await page.goto('https://jobboerse.arbeitsagentur.de/vamJB/stellenangeboteVerwalten.html?execution=e1s1', {timeout: 0});
        await page.screenshot({path: __dirname + '/../out/load_04.png'});
    }

    const url = await page.url();
    const cookies = await page.cookies();
    const content = await page.content();

    await browser.close();

    {
        const jobs = [];
        const domExplorer = cheerio.load(content);
        const jobEntries = domExplorer('table#tabellenanfang > tbody > tr');
        jobEntries.each(function (index: number, element: CheerioElement) {
            // get elements
            const jobNameNode = cheerio(`a#vermittlung\\.stellenangeboteverwalten\\.stellenangeboteubersicht\\.titeldesstellenangebots_${index}`, element).first();
            const jobIdNode = cheerio(`span#referenznummerdesstellenangebots_${index}`, element).first();
            const jobTypeNode = cheerio(`td:nth-child(2) > div`, element).first();
            const jobPublicationNode = cheerio(`td:nth-child(4) > div`, element).first();
            const jobLocationNode = cheerio(`td:nth-child(5) > div`, element).first();

            // extract element text
            const jobName = jobNameNode.text().trim();
            const jobId = jobIdNode.text().trim();
            const jobType = jobTypeNode.text().trim();
            const jobPublicationDate = jobPublicationNode.text().trim();
            const jobLocation = jobLocationNode.text().trim();

            const job = {
                name: jobName,
                id: jobId,
                offer_type: jobType,
                publication_date: jobPublicationDate,
                location: jobLocation
            };
            jobs.push(job);
        });
        console.log(JSON.stringify(jobs, null, 4));
    }

})();

